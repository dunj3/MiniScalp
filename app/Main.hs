module Main (main) where

import Data.Text
import MiniScalp.Predicates
import MiniScalp.Query
import MiniScalp.Sources
import MiniScalp.Types

data MensaLine = MensaLine Text [Text] deriving (Show)

source :: Text
source = "<!DOCTYPE html><html><div class=\"content\"><p>Hey <span id=\"username\">Alice</span>!</p></div></html>"

nameScraper :: Scraper Text
nameScraper = chroot ("div" @: [hasClass "content"]) $ chroot ("span" @: ["id" @= "username"]) text

main :: IO ()
main = do
  let scraped = scrapeText source nameScraper
  print scraped

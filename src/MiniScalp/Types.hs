-- |
-- Type definitions for MiniScalp.
--
-- The main type is a 'ScraperT', which provides the monadic interface to the scraper. It is a monad transformer, so you
-- can use it in combinations with other monads in your scrapers. If you do not need any other monads, you can use the
-- 'Scraper'.
module MiniScalp.Types
  ( -- * Scraper types
    ScraperT,
    Scraper,
    runScraperT,
    runScraper,

    -- * Auxiliary types
    ScrapeContext,
    Predicate,
  )
where

import Control.Applicative
import Control.Monad.Identity
import Control.Monad.Reader
import Control.Monad.Trans.Maybe
import Zenacy.HTML (HTMLNode)

-- | Context of the scraping operation
--
-- The first element represents the stack of parent nodes so we can inspect the predecessors of the current element.
-- Note that the stack is "reversed", meaning the first element is the immediate parent.
--
-- The second element represents the currently focussed node.
type ScrapeContext = ([HTMLNode], HTMLNode)

-- | Predicate to match HTML elements.
--
-- Gets passed the current stack as well as the current node.
type Predicate = [HTMLNode] -> HTMLNode -> Bool

-- | Main Monad of the scraper machinery.
newtype ScraperT m a = MkScraperT (ReaderT ScrapeContext (MaybeT m) a)
  deriving (Functor, Applicative, Alternative, Monad, MonadReader ScrapeContext, MonadPlus)

instance MonadTrans ScraperT where
  lift = MkScraperT . lift . lift

-- | Runs the given scraper.
runScraperT ::
  -- | The scraper to run.
  ScraperT m a ->
  -- | The initial HTML node (usually the document root).
  HTMLNode ->
  -- | The resulting scraped value.
  m (Maybe a)
runScraperT (MkScraperT s) n = runMaybeT $ runReaderT s ([], n)

-- | Alias for Scrapers that don't need an additional monadic context.
type Scraper a = ScraperT Identity a

-- | Analogue to 'runScraperT'.
runScraper :: Scraper a -> HTMLNode -> Maybe a
runScraper s n = runIdentity $ runScraperT s n

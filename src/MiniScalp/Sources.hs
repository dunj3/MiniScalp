-- | Various entry points for scrapers using different data sources.
module MiniScalp.Sources
  ( -- * Scraping in-memory text
    scrapeTextT,
    scrapeText,

    -- * Scraping local files
    scrapeFileT,
    scrapeFile,

    -- * Scraping remote URLs
    scrapeUrlT,
    scrapeUrl,
  )
where

import Data.Functor ((<&>))
import Data.Functor.Identity (runIdentity)
import Data.Text (Text)
import Data.Text.Encoding (decodeUtf8)
import Data.Text.IO qualified as T
import MiniScalp.Types
import Network.HTTP.Simple (getResponseBody, httpBS, parseRequest)
import Zenacy.HTML (htmlParseEasy)

-- | Parse and scrape the given 'Text'.
scrapeTextT :: Text -> ScraperT m a -> m (Maybe a)
scrapeTextT text scraper = runScraperT scraper $ htmlParseEasy text

-- | Specialised version of 'scrapeTextT'.
scrapeText :: Text -> Scraper a -> Maybe a
scrapeText text scraper = runScraper scraper $ htmlParseEasy text

-- | Read the file from the given path and scrape it.
scrapeFileT :: FilePath -> ScraperT m a -> IO (m (Maybe a))
scrapeFileT path scraper = T.readFile path <&> flip scrapeTextT scraper

-- | Specialised version of 'scrapeFileT'.
scrapeFile :: FilePath -> Scraper a -> IO (Maybe a)
scrapeFile path scraper = T.readFile path <&> flip scrapeText scraper

-- | Retrieve the file from the given URL.
scrapeUrlT :: String -> ScraperT m a -> IO (m (Maybe a))
scrapeUrlT url scraper = parseRequest url >>= \d -> (httpBS d <&> getResponseBody <&> decodeUtf8 <&> flip scrapeTextT scraper)

-- | Speccialised version of 'scrapeUrlT'.
scrapeUrl :: String -> Scraper a -> IO (Maybe a)
scrapeUrl url scraper = runIdentity <$> scrapeUrlT url scraper

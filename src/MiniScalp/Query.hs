-- | Data retrieval functions.
module MiniScalp.Query
  ( -- * Simple accessors
    node,
    parents,
    text,
    text',
    attribute,
    html,

    -- * Complex navigation
    retrieve,
    chroots,
    chroot,
  )
where

import Control.Applicative (empty, optional)
import Control.Monad (forM)
import Control.Monad.Reader (asks, local)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Text qualified as T
import MiniScalp.Types
import Zenacy.HTML

-- | Retrieves the current node.
node :: (Monad m) => ScraperT m HTMLNode
node = asks snd

-- | Retrieves the parents of the current node.
--
-- Node that the first entry is the immediate parent.
parents :: (Monad m) => ScraperT m [HTMLNode]
parents = asks fst

-- | Retrieves the text of the current node.
text :: (Monad m) => ScraperT m Text
text = node >>= \n -> maybe empty return $ htmlElemText n

-- | Recursively retrieves the text of the current node and all children nodes.
text' :: (Monad m) => ScraperT m Text
text' = recurseText <$> node
  where
    recurseText :: HTMLNode -> Text
    recurseText (HTMLDocument _ c) = T.concat $ map recurseText c
    recurseText (HTMLDoctype {}) = mempty
    recurseText (HTMLFragment _ c) = T.concat $ map recurseText c
    recurseText (HTMLElement _ _ _ c) = T.concat $ map recurseText c
    recurseText (HTMLTemplate {}) = mempty
    recurseText (HTMLText t) = t
    recurseText (HTMLComment _) = mempty

-- | Retrieves the value of the attribute with the given name.
--
-- Fails if the attribute does not exist.
attribute :: (Monad m) => Text -> ScraperT m Text
attribute a = node >>= \n -> maybe empty return $ htmlElemGetAttr a n

-- | Retrieves the rendered HTML of the current node.
--
-- Note that this may not correspond to the original source, as it is re-rendered from the DOM.
html :: (Monad m) => ScraperT m Text
html = htmlRender <$> node

-- | Retrieves all child contexts for which the given predicate matches.
retrieve :: (Monad m) => Predicate -> ScraperT m [ScrapeContext]
retrieve predicate = do
  n <- node
  ps <- parents
  let includeRoot = predicate ps n
  children <- concat <$> forM (htmlNodeContent n) (\child -> local (const (n : ps, child)) (retrieve predicate))
  return $ if includeRoot then (ps, n) : children else children

-- | Finds the elements according to the predicate and then executes the given scraper in their contexts.
--
-- If a subscraper fails, it is silently skipped.
chroots :: (Monad m) => Predicate -> ScraperT m a -> ScraperT m [a]
chroots p s = do
  es <- retrieve p
  catMaybes <$> forM es (\ctx -> local (const ctx) $ optional s)

-- | Like 'chroots', but only executes the scraper in the first context.
--
-- If no matching elements are found, this scraper fails.
chroot :: (Monad m) => Predicate -> ScraperT m a -> ScraperT m a
chroot p s = do
  cs <- chroots p s
  case cs of
    a : _ -> return a
    [] -> empty

-- | Various predicates to match HTML nodes.
module MiniScalp.Predicates
  ( tag,
    (@&),
    (@|),
    (@=),
    (@/),
    (@:),
    hasClass,
  )
where

import Data.List (tails)
import Data.Text (Text)
import MiniScalp.Types
import Zenacy.HTML

-- | Matches if both predicates match.
--
-- > tag "tr" @& hasClass "menu-title"
(@&) :: Predicate -> Predicate -> Predicate
a @& b = \s n -> a s n && b s n

infixl 8 @&

-- | Matches if one of the predicates matches.
--
-- > tag "thead" @| tag "tbody"
(@|) :: Predicate -> Predicate -> Predicate
a @| b = \s n -> a s n || b s n

infixl 7 @|

-- | Matches if the current node has the given tag.
--
-- > tag "p"
tag :: Text -> Predicate
tag t _ = htmlElemHasName t

-- | Matches if the current node has the given attribute and value.
--
-- > "id" @= "description"
(@=) :: Text -> Text -> Predicate
k @= v = \_ n -> htmlElemHasAttrVal k v n

infix 9 @=

-- | Matches if the node has the given class.
--
-- > hasClass "src"
hasClass :: Text -> Predicate
hasClass c _ = htmlElemClassesContains c

-- | Matches if the right predicate matches the current node, and the left operand matches a predecessor node.
--
-- > tag "p" @/ tag "img"
(@/) :: Predicate -> Predicate -> Predicate
a @/ b = \s n -> b s n && or (zipWith a (drop 1 $ tails s) s)

infixl 1 @/

-- | Shorthand to find a specific tag with the given predicates.
--
-- The following two are equivalent:
--
-- > "p" @: [hasClass "text", "id" @= "description"]
--
-- and
--
-- > tag "p" @& hasClass "text" @& "id" @= "description"
(@:) :: Text -> [Predicate] -> Predicate
t @: a = tag t @& \s n -> all (\p -> p s n) a

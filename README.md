# MiniScalp

This is my learning experiment to build a Haskell HTML scraping interface on
top of [`zenacy-html`](https://hackage.haskell.org/package/zenacy-html),
inspired by [`scalpel`](https://hackage.haskell.org/package/scalpel) but with
more correct (according to the spec) HTML parsing.

Don't expect this package to be in any form complete or production ready!

## Example

```haskell
module Main (main) where

import Control.Monad
import Data.Maybe
import Data.Text
import MiniScalp.Predicates
import MiniScalp.Query
import MiniScalp.Sources
import MiniScalp.Types

data MensaLine = MensaLine Text [Text] deriving (Show)

mensaScraper :: Scraper [MensaLine]
mensaScraper = chroot ("id" @= "canteen_day_1") $ do
  chroots (tag "tr" @& hasClass "mensatype_rows") $ do
    name <- chroot ("td" @: [hasClass "mensatype"]) text'
    meals <- chroots ("td" @: [hasClass "menu-title"]) text'
    return $ MensaLine name meals

main :: IO ()
main = do
  scraped <- fromJust <$> scrapeFile "mensa.html" mensaScraper
  forM_ scraped $ \(MensaLine name meals) -> do
    putStrLn $ unpack name
    forM_ meals $ \meal -> putStrLn ("  " ++ unpack meal)
    putStrLn ""
```

## Licence

Copyright © 2023 Dunj3

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
